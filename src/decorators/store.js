export const store = options => target => {

  const instance = new target

  const obj = {
    ...options,
    state: {},
    mutations: {},
    actions: {},
    getters: {}
  };

  if(instance.___stateVars != undefined) {
    instance.___stateVars.forEach(prop => obj.state[prop] = instance[prop])
  }

  if(instance.___mutations != undefined) {
    instance.___mutations.forEach(mutation => obj.mutations[mutation] = (state, arg) => instance[mutation].call(state, arg))
  }

  if(instance.___actions != undefined) {
    instance.___actions.forEach(action => obj.actions[action] = (context, arg) => instance[action].call(context, arg))
  }

  Object.getOwnPropertyNames(target.prototype).filter(pn => Object.getOwnPropertyDescriptor(target.prototype, pn).get).forEach(
      getter => obj.getters[getter] = () => Object.getOwnPropertyDescriptor(target.prototype, getter).get.call(obj.state)
  )

  Object.getOwnPropertyNames(target.prototype).filter(pn => Object.getOwnPropertyDescriptor(target.prototype, pn).set).forEach(
      setter => obj.mutations[setter] = (state, arg) => Object.getOwnPropertyDescriptor(target.prototype, setter).set.call(obj.state, arg)
  )

  return obj
}

export const state = function(target, property) {
  if(target.___stateVars === undefined) {
    target.___stateVars = [];
  }

  target.___stateVars.push(property)
}

export const mutation = function(target, method) {
  if(target.___mutations === undefined) {
    target.___mutations = [];
  }

  target.___mutations.push(method)
}

export const action = function(target, method) {
  if(target.___actions === undefined) {
    target.___actions = [];
  }

  target.___actions.push(method)
}
