import Vue from 'vue'
import Vuex from 'vuex'

import store from "./store"
console.log(store)
Vue.use(Vuex)

export default new Vuex.Store({
  strict: true,
  modules: {
    store
  }
})
