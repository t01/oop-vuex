import {store, state, mutation, action, getter} from '../decorators/store.js'

@store({
  namespaced: true
})
export default class oopStore {
  @state
  v = "test"

  get val() {
    return this.v + 'X';
  }

  set val(v) {
    this.v = v + 'Z';
  }

  @mutation
  mutateValue(v) {
    this.v = v + 'Y';
  }

  @action
  async setValue(v) {console.log(this.commit)
    this.commit('val', v);
  }
}
